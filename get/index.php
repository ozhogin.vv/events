<?php

require '../classes.php';

if (empty($_GET['type'])) {
    header("HTTP/1.0 404 Not Found");
    exit(); 
}

$type = $_GET['type'];

$aggr_types = ['name', 'user_ip', 'user_status'];

if (!in_array($type, $aggr_types)) {
    header("HTTP/1.0 404 Not Found");
    exit(); 
} 

$db = new DB("127.0.0.1", "events", "root", ""); 
$conn = $db->connect();

$filters = array();

if (!empty($_GET['name'])) {
    $filters += ['name'=>$_GET['name']]; 
}
if (!empty($_GET['date'])) {
    $filters += ['date'=>$_GET['date']]; 
}

echo EventsRepository::getAggregatedData($conn, $type, $filters);
