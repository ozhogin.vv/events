<?php

require '../classes.php';

if (empty($_GET['name']) || empty($_GET['user_status'])) {
    header("HTTP/1.0 404 Not Found");
    exit(); 
} 

$user_status = $_GET['user_status'];

if ($user_status == 1 || $user_status == 2) {
    $db = new DB("127.0.0.1", "events", "root", ""); 
    $conn = $db->connect();

    $event_name = $_GET['name'];
    $event = new Event($event_name, $user_status);
    EventsHelper::addInfo($event);
    EventsRepository::saveEvent($conn, $event);
} else {
    header("HTTP/1.0 404 Not Found");
    exit(); 
}
