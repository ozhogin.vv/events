<?php

class Event
{
    protected $name;
    protected $user_status;
    protected $date;
    protected $user_ip;

    public function __construct($name, $user_status)
    {
        $this->name = $name;
        $this->user_status = $user_status;
    }

    public function __get($property)
    {
        switch ($property)
        {
            case 'name':
                return $this->name;
            case 'user_status':
                return $this->user_status;
            case 'date':
                return $this->date;
            case 'user_ip':
                return $this->user_ip;
        }
    }
 
    public function __set($property, $value)
    {
        switch ($property)
        {
            case 'name':
                $this->name = $value;
                break;
            case 'user_status':
                $this->user_status = $value;
                break;
            case 'date':
                $this->date = $value;
                break;
            case 'user_ip':
                $this->user_ip = $value;
                break;
        }
    }
}

class EventsHelper
{
    static function addInfo(Event $event) {
        $event->user_ip = $_SERVER['REMOTE_ADDR'];
        $event->date = date('Y-m-d');
    }
}

class EventsRepository 
{
    static function saveEvent($conn, Event $event) {
        $sql = "INSERT INTO events (name, user_status, date, user_ip) VALUES ('$event->name', '$event->user_status', '$event->date', '$event->user_ip')"; 
        mysqli_query($conn, $sql);
    }

    static function getAggregatedData($conn, $type, $filters = []) {
        $sql = "SELECT MAX($type) AS $type, COUNT($type) AS Count FROM events WHERE";
        foreach ($filters as $filter => $value) {
            $sql = $sql . " `$filter` = '$value' AND";
        }
        $sql = substr($sql, 0, strlen($sql) - 4) . " GROUP BY ($type)";
        $result = $conn->query($sql);
        $rows = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        return json_encode($rows);
    }
}

class DB
{
    protected $servername;
    protected $database;
    protected $username; 
    protected $password;

    public function __construct($servername, $database, $username, $password) {
        $this->servername = $servername;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
    }

    public function connect() {
        return new mysqli($this->servername, $this->username, $this->password, $this->database);
    }
}
